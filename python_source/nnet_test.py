"""
Copyright 2016 Clayton Kotulak, All Rights Reserved

"""

import tensorflow as tf
import nnet_node
import nnet_graph
import nnet_trainer
import nnet_ffnet
import nnet_cluster
import input_data
import numpy


# Model parameters as flags
flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_integer('batch_size', 50, 'Number of instances per batch.')
flags.DEFINE_integer('max_steps', 36000, 'Number of steps to run trainer.')
flags.DEFINE_float('learning_rate', 1e-4, 'Initial learning rate.')
flags.DEFINE_float('keep_prob', 0.5, 'Output dropout probability.')
flags.DEFINE_integer('target_label', 8, 'Target label for binary one-hot.')
flags.DEFINE_string('train_dir', 'data', 'Directory to store training logs.')


if __name__ == "__main__":

  data_sets = input_data.read_data_sets(FLAGS.train_dir, one_hot=True,
                                          target_label=FLAGS.target_label)
  excite_n = numpy.sum(data_sets.train.labels == 0)
  inhibit_n = numpy.sum(data_sets.train.labels == 1)
  sample_ratio = excite_n / (excite_n + inhibit_n)

  # Network architecture
  hyper_params = {'input_features': data_sets.train.num_features,
                  'output_classes': data_sets.train.num_classes,
                  'sample_ratio': sample_ratio,
                  'hidden_nodes': [],
                  'max_nodes': 4,
                  'node_steps': 4500,
                  'batch_size': FLAGS.batch_size,
                  'max_steps': FLAGS.max_steps,
                  'learning_rate': FLAGS.learning_rate,
                  'keep_prob': FLAGS.keep_prob}


  if 0:
    nnet_graph = nnet_graph.NNetGraph("baseline", hyper_params)
    nnet_graph.print_layers()
    nnet_trainer = nnet_trainer.NNetTrainer("baseline", nnet_graph, hyper_params)
    nnet_trainer.run_training(data_sets.train)
  if 0:
    node_graph = nnet_node.NNetFactory(data_sets.train, hyper_params)
    #weights, bias, logits = node_graph.train_node(node_idx, node_labels)
    #out_list = numpy.hstack( (logits, node_labels.reshape((sample_n,1))) )
    #print(out_list)

  cluster_layer = nnet_cluster.NNetCluster(data_sets.train, hyper_params)
  #test_net = nnet_ffnet.FFNetwork('test_net', data_sets, hyper_params)
