# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""
Functions for downloading and reading MNIST data.
Modified version of mnist_data.py from tensorflow MNIST tutorials

Updated functionality:
    read_data_sets now takes an additional argument target_label specifying
    which label (0-9) to isolate from all other label classes. Labels in the
    returned object data_sets are now a one-hot encoding of two classes, the
    null class (all labels but the target label), and target class respectively.

    read_mdata_sets has been added to import the training data from a matlab
    data file.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import gzip
import os
import numpy
import math

from scipy.special import expit
from six.moves import urllib
from six.moves import xrange  # pylint: disable=redefined-builtin

SOURCE_URL = 'http://yann.lecun.com/exdb/mnist/'


def maybe_download(filename, work_directory):
  """Download the data from Yann's website, unless it's already here."""
  if not os.path.exists(work_directory):
    os.mkdir(work_directory)
  filepath = os.path.join(work_directory, filename)
  if not os.path.exists(filepath):
    filepath, _ = urllib.request.urlretrieve(SOURCE_URL + filename, filepath)
    statinfo = os.stat(filepath)
    print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
  return filepath


def _read32(bytestream):
  dt = numpy.dtype(numpy.uint32).newbyteorder('>')
  return numpy.frombuffer(bytestream.read(4), dtype=dt)[0]


def extract_images(filename):
  """Extract the images into a 4D uint8 numpy array [index, y, x, depth]."""
  print('Extracting', filename)
  with gzip.open(filename) as bytestream:
    magic = _read32(bytestream)
    if magic != 2051:
      raise ValueError(
          'Invalid magic number %d in MNIST image file: %s' %
          (magic, filename))
    num_images = _read32(bytestream)
    rows = _read32(bytestream)
    cols = _read32(bytestream)
    buf = bytestream.read(rows * cols * num_images)
    data = numpy.frombuffer(buf, dtype=numpy.uint8)
    data = data.reshape(num_images, rows, cols, 1)
    return data


def dense_to_one_hot(labels_dense, num_classes=10):
  """Convert class labels from scalars to one-hot vectors."""
  num_labels = labels_dense.shape[0]
  index_offset = numpy.arange(num_labels) * num_classes
  labels_one_hot = numpy.zeros((num_labels, num_classes))
  labels_one_hot.flat[index_offset + labels_dense.ravel()] = 1
  return labels_one_hot


def dense_to_binary(labels_dense, target_label):
  """Convert class labels from multi-class scalars to binary scalars."""
  return (numpy.equal(labels_dense, target_label)).astype(int)


def dense_to_binary_oneh(labels_dense, target_label):
  """Convert class labels from scalars to binary one-hot matrix."""
  labels_target = (numpy.equal(labels_dense, target_label)).astype(int)
  labels_null = (numpy.not_equal(labels_dense, target_label)).astype(int)
  labels_bin_one_hot = numpy.column_stack((labels_null, labels_target))
  return labels_bin_one_hot


def extract_labels(filename, one_hot=False):
  """Extract the labels into a 1D uint8 numpy array [index]."""
  print('Extracting', filename)
  with gzip.open(filename) as bytestream:
    magic = _read32(bytestream)
    if magic != 2049:
      raise ValueError(
          'Invalid magic number %d in MNIST label file: %s' %
          (magic, filename))
    num_items = _read32(bytestream)
    buf = bytestream.read(num_items)
    labels = numpy.frombuffer(buf, dtype=numpy.uint8)
    if one_hot:
      return dense_to_binary_oneh(labels)
    return labels


def read_data_sets(train_dir, target_label=0, one_hot=False):
  class DataSets(object):
    pass
  data_sets = DataSets()

  TRAIN_IMAGES = 'train-images-idx3-ubyte.gz'
  TRAIN_LABELS = 'train-labels-idx1-ubyte.gz'
  TEST_IMAGES = 't10k-images-idx3-ubyte.gz'
  TEST_LABELS = 't10k-labels-idx1-ubyte.gz'
  VALIDATION_SIZE = 5000

  local_file = maybe_download(TRAIN_IMAGES, train_dir)
  train_images = extract_images(local_file)

  # Extract labels and apply binary class format
  local_file = maybe_download(TRAIN_LABELS, train_dir)
  train_labels = extract_labels(local_file, one_hot=False)
  train_labels = dense_to_binary(train_labels, target_label)

  local_file = maybe_download(TEST_IMAGES, train_dir)
  test_images = extract_images(local_file)

  local_file = maybe_download(TEST_LABELS, train_dir)
  test_labels = extract_labels(local_file, one_hot=False)
  test_labels = dense_to_binary(test_labels, target_label)

  validation_images = train_images[:VALIDATION_SIZE]
  validation_labels = train_labels[:VALIDATION_SIZE]

  # Commented: Exclude validation images from training set
  train_images = train_images[VALIDATION_SIZE:]
  train_labels = train_labels[VALIDATION_SIZE:]

  data_sets.train = DataSet(train_images, train_labels)
  data_sets.validation = DataSet(validation_images, validation_labels)
  data_sets.test = DataSet(test_images, test_labels)

  return data_sets


class DataSet(object):

  def __init__(self, images, labels, one_hot=False):
    """Construct a DataSet. one_hot arg is used only if fake_data is true."""
    assert images.shape[0] == labels.shape[0], (
        'images.shape: %s labels.shape: %s' % (images.shape,
                                               labels.shape))

    # Convert shape from [num examples, rows, columns, depth]
    # to [num examples, rows*columns] (assuming depth == 1)
    if len(images.shape) > 2:
      assert images.shape[3] == 1
      images = images.reshape(images.shape[0],
                              images.shape[1] * images.shape[2])

    # Convert from [0, 255] -> [0.0, 1.0].
    images = images.astype(numpy.float32)
    images = numpy.multiply(images, 1.0 / 255.0)

    self._images = images
    self._labels = labels
    self._index_perm = numpy.arange(images.shape[0])
    self._num_examples = images.shape[0]
    self._num_classes = numpy.amax(labels) + 1
    self._num_features = images.shape[1]
    self._epochs_completed = 0
    self._index_in_epoch = 0

  @property
  def images(self):
    return self._images

  @property
  def labels(self):
    return self._labels

  @property
  def num_examples(self):
    return self._num_examples

  @property
  def num_classes(self):
    return self._num_classes

  @property
  def num_features(self):
    return self._num_features

  @property
  def epochs_completed(self):
    return self._epochs_completed

  def get_examples(self, start_idx, end_idx):
    return (self._images[start_idx:end_idx],
           self._labels[start_idx:end_idx])

  def get_weighted_subset(self, sample_weights):
    return WeightedSubset(self, sample_weights)

  def index_batch(self, batch_size, index_perm):
    """Return the next `batch_size` examples from this data set."""
    example_n = index_perm.shape[0]
    if batch_size > example_n:
      random_idx = numpy.random.randint(0, example_n, batch_size)
      return index_perm[random_idx]
    start = self._index_in_epoch
    self._index_in_epoch += batch_size
    if self._index_in_epoch > example_n:
      # Epoch finished
      self._epochs_completed += 1
      # Shuffle the example indices to randomize batch selection
      numpy.random.shuffle(index_perm)
      start = 0
      self._index_in_epoch = batch_size
    end = self._index_in_epoch
    selected_idx = index_perm[start:end]
    return selected_idx

  def next_batch(self, batch_size):
    selected_idx = self.index_batch(batch_size, self._index_perm)
    weight_placeholder = numpy.ones(batch_size)
    return (self.images[selected_idx],
            self.labels[selected_idx],
            weight_placeholder)

  def replace_images(self, new_images):
    assert self._images.shape[0] == new_images.shape[0]
    self._old_images = self._images
    self._images = new_images


class WeightedSubset(object):
  def __init__(self, data_set, data_weights):
    self._data_set = data_set
    self._index_perm = numpy.arange(self._data_set.num_examples)
    self._weights = data_weights

  @property
  def weights(self):
    return self._weights

  def next_batch(self, batch_size):
    selected_indices = numpy.random.randint(0, self._data_set.num_examples,
                                            batch_size)
    return (self._data_set.images[selected_indices],
           self._data_set.labels[selected_indices],
           self.weights[selected_indices])
