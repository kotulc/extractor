# Copyright 2016 Clayton Kotulak, All Rights Reserved
#
# nnet_trainer.py
# Functionality for initializing and training a neural network graph
# with TensorFlow.

"""
Note: Some functionality adapted from TensorFlow tutorial files
"""

import tensorflow as tf
import time
import sys


class NNetTrainer(object):

  def __init__(self, id, nnet_graph, hyper_params, eval_bool=True):
    # Use id as the directory name to store object data
    self.id = id
    self.nnet_graph = nnet_graph
    #self.sample_ratio = hyper_params['sample_ratio']
    self.batch_n = hyper_params['batch_size']
    self.max_steps = hyper_params['max_steps']
    self.learning_rate = hyper_params['learning_rate']
    self.keep_prob = hyper_params['keep_prob']
    self.eval_bool = eval_bool

    with self.nnet_graph.tf_graph.as_default():
      # Build the graph with all operations required for training
      self.loss = self._graph_bin_xloss(self.nnet_graph)
      self.train_op = self._graph_training(self.loss)
      if self.eval_bool == True:
        self.eval_correct = self._graph_evaluation(self.nnet_graph.logits,
                                                  self.nnet_graph.y)

  # Calculate the loss by comparing logits and labels
  def _graph_hloss(self, node_graph):
    with tf.name_scope('huber_loss'):
      logits = tf.reshape(node_graph.logits, [-1])
      residuals = tf.sub(tf.to_float(node_graph.y), logits)
      s_const = tf.constant(2.0, name='huber_const')
      one_const = tf.constant(1.0, name='one_const')
      actv_term = tf.square(tf.div(residuals, s_const))
      sqrt_term = tf.sqrt(tf.add(one_const, actv_term))
      prim_term = tf.mul(tf.square(s_const), tf.sub(sqrt_term, one_const))
      loss = tf.reduce_mean(prim_term, name='hcost_mean')
    return loss

  # Calculate the loss by comparing logits and labels
  def _graph_xloss(self, node_graph):
    labels = tf.to_int64(node_graph.y)
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            node_graph.logits, labels, name='xentropy')
    loss = tf.reduce_mean(cross_entropy, name='xcost_mean')
    return loss

  def _graph_bin_xloss(self, node_graph):
    labels = tf.to_int64(node_graph.y)
    #class_weight = tf.constant([[self.sample_ratio, 1.0 - self.sample_ratio]])
    #weight_per_label = tf.matmul(labels, node_graph.weights)
    #weight_per_label = weight_per_label.transpose()
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
            node_graph.logits, labels, name='xentropy')
    cross_entropy = tf.mul(node_graph.weights, cross_entropy)
    loss = tf.reduce_mean(cross_entropy, name='xcost_mean')
    return loss

  # Return the optimizer operation for this model
  def _graph_training(self, loss):
    tf.scalar_summary(loss.op.name, loss)
    optimizer = tf.train.AdamOptimizer(self.learning_rate)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    train_op = optimizer.minimize(loss, global_step=global_step)
    return train_op

  # Return a tensor with the number of instances correctly predicted
  def _graph_evaluation(self, logits, labels):
    correct_prediction = tf.nn.in_top_k(logits, labels, 1)
    return tf.reduce_sum(tf.cast(correct_prediction, 'float'))

  # Runs a single evaluation against a full epoch of data
  def evaluate_model(self, sess, data_set, keep_prob):
    true_count = 0
    steps_per_epoch = data_set.num_examples // self.batch_n
    example_n = steps_per_epoch * self.batch_n
    for step in xrange(steps_per_epoch):
      feed_dict = self.fill_feed_dict(data_set, keep_prob)
      true_count += sess.run(self.eval_correct, feed_dict=feed_dict)
    precision = float(true_count) / example_n
    print("Num examples: %d  Num correct: %d  Precision @ 1: %0.04f\n" %
          (example_n, true_count, precision))

  # Fill_feed_dict from fully_connected_feed.py
  def fill_feed_dict(self, data_set, keep_prob=None):
    x_feed, y_feed, w_feed = data_set.next_batch(self.batch_n)
    assert x_feed.shape[0] == self.batch_n
    feed_dict = {self.nnet_graph.x: x_feed,
                 self.nnet_graph.y: y_feed,
                 self.nnet_graph.weights: w_feed,
                 self.nnet_graph.keep_prob: keep_prob}
    return feed_dict

  # Create the session, initialize variables, and run training loop
  def run_training(self, train_data, val_data=None, session=None, save=True):
    close_session = session is None
    with self.nnet_graph.tf_graph.as_default():
      summary_op = tf.merge_all_summaries()
      if session==None:
        session = tf.Session()
      if save == True:
        #saver = tf.train.Saver()
        summary_writer = tf.train.SummaryWriter('temp/'+self.id,
                                                session.graph)
      # Add and run the Op to initialize the variables
      init = tf.initialize_all_variables()
      session.run(init)

      for step in xrange(self.max_steps):
        start_time = time.time()
        feed_dict = self.fill_feed_dict(train_data, self.keep_prob)
        _, loss_value = session.run([self.train_op, self.loss],
                                        feed_dict=feed_dict)
        duration = time.time() - start_time
        if step%500 == 0:
          # Print status to stdout
          sys.stdout.write('Step: %d, loss=%.4f (%.3f sec)\r' %
                         (step, loss_value, duration))
          sys.stdout.flush()
          if save == True:
            summary_str = session.run(summary_op, feed_dict=feed_dict)
            summary_writer.add_summary(summary_str, step)
            summary_writer.flush()
        # Evaluate the model after training
        if ((step + 1) >= self.max_steps) and self.eval_bool:
          #saver.save(sess, self.id +'/nnet.ckpt')
          print("Training data evaluation:")
          self.evaluate_model(session, train_data, 1.0)
          if val_data != None:
            print("Validation data evaluation:")
            self.evaluate_model(session, val_data, 1.0)

      if close_session:
        session.close()