# Copyright 2016 Clayton Kotulak, All Rights Reserved
#
# Wraper class for generating and managing a TensorFlow neural network graph.

"""
Note: Some functionality adapted from TensorFlow tutorial files
"""

import tensorflow as tf
import nnet_trainer as nntrainer
import nnet_graph as nngraph
import numpy
import math


class NNetFactory(object):

  def __init__(self, train_data, hyper_params):
    # Build node graph
    self.train_data = train_data
    self.batch_n = hyper_params['batch_size']
    node_params = hyper_params.copy()
    node_params['max_steps'] = node_params['node_steps']
    layer_type = 'node'
    self.node_graph = nngraph.NNetGraph('node', node_params,
                                        layer_type=layer_type)
    self.trainer = nntrainer.NNetTrainer('node', self.node_graph,
                                         node_params, eval_bool=False)

  def train_node(self, sample_weights):
    # Train the node on the select indices
    with self.node_graph.tf_graph.as_default():
      sess = tf.Session()
    weighted_data = self.train_data.get_weighted_subset(sample_weights)
    self.trainer.run_training(weighted_data, session=sess)
    # Get the bias and weights for this node
    node = NNetNode(self.node_graph, sess)
    bias = node.bias_val
    weights = node.weight_val
    # Calculate the logits and split between inhibitory and excitatory
    logits = self.node_graph.get_logits(sess, self.train_data, self.batch_n)
    self.print_stats(logits, sample_weights)
    sess.close()
    return bias, weights, logits

  def print_stats(self, logits, sample_weights):
    excite_actvs = logits[self.train_data.labels == 1]
    inhibit_actvs = logits[self.train_data.labels == 0]
    sample_weights = numpy.reshape(sample_weights, (logits.shape[0], 1))
    logits_weighted = numpy.multiply(logits, sample_weights)
    e_weighted = logits_weighted[self.train_data.labels == 1]
    i_weighted = logits_weighted[self.train_data.labels == 0]
    eactvs_sum = float(numpy.sum(excite_actvs))
    iactvs_sum = float(numpy.sum(inhibit_actvs))
    e_weighted_sum = float(numpy.sum(e_weighted))
    i_weighted_sum = float(numpy.sum(i_weighted))
    eactv_avg = float(eactvs_sum / sum(self.train_data.labels == 1))
    iactv_avg = float(iactvs_sum / sum(self.train_data.labels == 0))
    egtr_avg = int(numpy.sum(excite_actvs > eactv_avg))
    igtr_avg = int(numpy.sum(inhibit_actvs > eactv_avg))
    format_str = "e/i actv_sum: %.2f/%.2f, weighted: %.2f/%.2f, "
    format_str += "avg: %.2f/%.2f, gtr_eavg: %d/%d"
    print(format_str % (eactvs_sum, iactvs_sum, e_weighted_sum, i_weighted_sum,
                        eactv_avg, iactv_avg, egtr_avg, igtr_avg))
    if eactvs_sum == 0:
      print("\nDead node\n")
      assert eactvs_sum != 0


class NNetNode(object):

  def __init__(self, graph, session=None):
    self._graph = graph
    self._bias_var = self._graph.bias_array[0]
    self._weights_var = self._graph.weight_array[0]
    self._session = session

  @property
  def bias_val(self):
    return self._session.run(self._bias_var)

  @property
  def weight_val(self):
    return self._session.run(self._weights_var)

  def get_logits(self, x):
    feed_dict = {self._graph.x: x, self._graph.keep_prob: 1.0}
    logits = self._session.run(self._graph.actv_array[0], feed_dict=feed_dict)
    return logits