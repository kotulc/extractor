# Copyright 2016 Clayton Kotulak, All Rights Reserved
#
# nnet_graph.py
# Wraper class for generating and managing a TensorFlow neural network graph.

"""
Note: Some functionality adapted from TensorFlow tutorial files
"""

import tensorflow as tf
import numpy
import math


class NNetGraph(object):

  def __init__(self, id, hyper_params, layer_type='elu'):
    # Use id as the directory name to store object data
    self.id = id
    self.feature_n = hyper_params['input_features']
    self.output_n = hyper_params['output_classes']
    self.hidden_nodes = hyper_params['hidden_nodes']
    self.dropout = hyper_params['keep_prob'] < 1
    self.layer_n = len(self.hidden_nodes) + 1
    self.bias_array = [None] * self.layer_n
    self.weight_array = [None] * self.layer_n
    self.actv_array = [None] * self.layer_n
    self.tf_graph = tf.Graph()

    with self.tf_graph.as_default():
      # Generate placeholders for the data
      self.x = tf.placeholder(tf.float32, shape=(None, self.feature_n),
                              name='input')
      self.y = tf.placeholder(tf.int32, shape=(None), name='labels')
      self.weights = tf.placeholder(tf.float32, shape=(None), name='weights')
      self.keep_prob = tf.placeholder('float', name='keep_prob')
      if layer_type == 'node':
        self.output_n = 1
        self.logits = self._graph_node(self.x)
      else:
        # Graph the output layer
        if len(self.hidden_nodes) == 0:
          logits = self.x
          output_features = self.feature_n
        else:
          # Graph the hidden layer inference
          logits = self._graph_inference(self.x, self.feature_n,
                                         self.hidden_nodes, layer_type)
          output_features = self.hidden_nodes[-1]
        # Graph the output layer
        self.logits = self.output_layer(logits, output_features,
                                        self.output_n, layer_type)

  # Iterate through the layer dimensions included in net_architecture to add
  # each neural network layer operation to the graph
  def _graph_inference(self, x, feature_n, layer_nodes, layer_type):
    layer_h = x
    layer_n = len(layer_nodes) + int(self.dropout)
    for layer_idx in xrange(0, layer_n):
      # Add dropout to the final layer of the network if enabled
      if self.dropout and (layer_idx == layer_n-1):
        layer_h = self.drop_features(layer_h, self.keep_prob)
      else:
        layer_h = self.fc_layer(layer_idx, layer_h, feature_n,
                                layer_nodes[layer_idx], layer_type, layer_idx)
        # Update the number of features for the next layer
        feature_n = layer_nodes[layer_idx]
    return layer_h

  def _graph_node(self, x):
    with tf.name_scope('fc_node'):
      self.b_fc = self.bias_variable([1], 0)
      self.w_fc = self.weight_variable([self.feature_n, 1], 0)
      drop_h = self.drop_features(x, self.keep_prob)
      node_h = self.node_operation(drop_h, self.b_fc, self.w_fc, 0, 'elu')
      out_h = tf.mul(node_h, [-1, 1])
    return out_h

  # Calculate and convert logits to numpy arrays
  def get_logits(self, session, data_set, batch_n):
    logits = numpy.zeros(shape=(data_set.num_examples, self.output_n))
    num_batches = math.ceil(data_set.num_examples/batch_n)
    for batch_idx in xrange(int(num_batches)):
      start_idx = batch_n * batch_idx
      end_idx = start_idx + batch_n
      if end_idx >= data_set.num_examples:
        end_idx = data_set.num_examples
      x, y = data_set.get_examples(start_idx, end_idx)
      feed_dict = {self.x: x, self.keep_prob: 1.0}
      batch_logits = session.run(self.actv_array[-1], feed_dict=feed_dict)
      logits[start_idx:end_idx] = batch_logits
    return logits

  def get_weights(self, session, layer_idx):
    assert layer_idx <= len(self.bias_array)
    biases = session.run(self.bias_array[layer_idx])
    weights = session.run(self.weight_array[layer_idx])
    return biases, weights

  def print_layers(self):
    layer_str = '[in]' + str(self.feature_n) + ' [h]'
    hidden_str = '-'.join(str(x) for x in self.hidden_nodes)
    layer_str = layer_str + hidden_str + ' [out]' + str(self.output_n)
    print("\n" + self.id + " network layers: " + layer_str)

  def print_weights(self, session, layer_start=0, layer_end=1):
    for layer_idx in xrange(layer_start, layer_end):
      biases, weights = self.get_weights(session, layer_idx)
      print("layer %d biases: %s" % (layer_idx, str(biases)))
      print("weights: %s\n" % str(weights))

  # Return a bias variable initialized with the given shape
  def bias_variable(self, shape, layer_idx):
      #initial = tf.constant(0.1, shape=shape)
      initial = tf.truncated_normal(shape, stddev=0.1)
      b_variable = tf.Variable(initial, name='biases')
      self.bias_array[layer_idx] = b_variable
      return b_variable

  # Return a weight variable initialized with the given shape
  def weight_variable(self, shape, layer_idx):
      initial = tf.truncated_normal(shape, stddev=0.1)
      w_variable = tf.Variable(initial, name='weights')
      self.weight_array[layer_idx] = w_variable
      return w_variable

  # Return the output of x with dropout applied
  def drop_features(self, x, keep_prob):
    with tf.name_scope('dropout'):
      # Apply dropout to x
      h_drop = tf.nn.dropout(x, keep_prob)
    return h_drop

  # Return the output of a fully connected elu layer
  def fc_layer(self, id, x, feature_n, node_n, node_type, layer_idx):
    with tf.name_scope('fc_'+str(id)):
      b_fc = self.bias_variable([node_n], layer_idx)
      w_fc = self.weight_variable([feature_n, node_n], layer_idx)
      # Apply operation type to x
      h_fc = self.node_operation(x, b_fc, w_fc, layer_idx, node_type)
      return h_fc

  def filter_layer(self, id, x, filter_layer, node_type='sigmoid'):
    with tf.name_scope('filter_'+str(id)):
      b_filter = tf.Variable(filter_layer[0], name='biases',
                             dtype=tf.float32, trainable=False)
      w_filter = tf.Variable(filter_layer[1], name='weights',
                             dtype=tf.float32, trainable=False)
      # Apply node operation type to x
      h_fc = self.node_operation(x, b_filter, w_filter, 0, node_type)
      return h_fc

  def node_operation(self, x, b_var, w_var, layer_idx, node_type='elu'):
    if node_type == 'sigmoid':
      h_node = tf.nn.sigmoid(tf.matmul(x, w_var) + b_var)
    elif node_type == 'softmax':
      h_node = tf.nn.softmax(tf.matmul(x, w_var) + b_var)
    else:
      h_node = tf.nn.elu(tf.matmul(x, w_var) + b_var)
    self.actv_array[layer_idx] = h_node
    return h_node

  # Return the output of the readout layer
  def output_layer(self, x, feature_n, output_n, node_type):
    with tf.name_scope('readout'):
      layer_idx = self.layer_n - 1
      b_out = self.bias_variable([output_n], layer_idx)
      w_out = self.weight_variable([feature_n, output_n], layer_idx)
      y = self.node_operation(x, b_out, w_out, layer_idx, node_type)
      return y