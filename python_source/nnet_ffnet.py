"""
Copyright 2016 Clayton Kotulak, All Rights Reserved

"""

import nnet_trainer as trainer
import nnet_graph as nngraph
import nnet_cluster as nncluster
import numpy


class FFNetwork(object):

  def __init__(self, id, data_sets, hyper_params):
    self.id = id

    if hyper_params['cluster_filter'] == True:
      cluster_layer = nncluster.NNetCluster(data_sets.train, hyper_params)

  def run_training(self, trainer, train_data, val_data=None):
    print("\nFFNetwork training...")
    trainer.run_training(train_data, val_data)