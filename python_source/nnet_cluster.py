# Copyright 2016 Clayton Kotulak, All Rights Reserved
#
# Wraper class for generating and managing a TensorFlow neural network graph.

"""
Note: Some functionality adapted from TensorFlow tutorial files
"""

import tensorflow as tf
import nnet_trainer as trainer
import nnet_graph as nngraph
import nnet_node as nnode
import numpy
import math


class NNetCluster(object):

  def __init__(self, train_data, hyper_params):
    self.train_data = train_data
    self.max_nodes = hyper_params['max_nodes']
    self.layer_n = len(hyper_params['hidden_nodes']) + 1
    feature_n = hyper_params['input_features']
    self.batch_n = hyper_params['batch_size']
    # Initialize the cluster node variables
    self.node_count = 0
    self.cluster_graph = None
    self.node_actvs = numpy.zeros((self.max_nodes, train_data.num_examples))
    self.weights = numpy.zeros((self.max_nodes, feature_n, 1))
    self.biases = numpy.zeros((self.max_nodes, 1))
    # Initialize the graph to train and generate nodes
    self.node_factory = nnode.NNetFactory(self.train_data, hyper_params)
    self._build_cluster()
    self._build_graph(hyper_params)

  # Iteratively generate cluster nodes
  def _build_cluster(self):
    excite_mask = self.train_data.labels == 1
    for node_idx in xrange(self.max_nodes):
      sample_weights = self._sample_weights(excite_mask)
      self._generate_node(sample_weights)
      excite_mask = self._reduce_mask(excite_mask)
      if (excite_mask.sum() == 0):
        break

  def _build_graph(self, hyper_params):
    # Generate the graph, trainer and train the node
    id = 'cluster'
    hyper_params['input_features'] = self.node_count
    self.cluster_graph = nngraph.NNetGraph(id, hyper_params)
    self.cluster_graph.print_layers()
    cluster_trainer = trainer.NNetTrainer(id, self.cluster_graph, hyper_params)
    new_images = self.node_actvs[0:self.node_count].transpose()
    self.train_data.replace_images(new_images)
    with self.cluster_graph.tf_graph.as_default():
      sess = tf.Session()
    cluster_trainer.run_training(self.train_data, session=sess)
    self.cluster_graph.print_weights(sess, 0, self.layer_n)
    logits = self.cluster_graph.get_logits(sess, self.train_data, self.batch_n)
    logits = logits.reshape((logits.shape[1], logits.shape[0]))
    self.print_cluster(logits, self.weights)
    sess.close()

  def _generate_node(self, sample_weights):
    if self.node_count >= self.max_nodes:
      return
    # Train the node on the select indices
    bias, weights, logits = self.node_factory.train_node(sample_weights)
    self.biases[self.node_count] = bias
    self.weights[self.node_count] = weights
    self.node_actvs[self.node_count] = logits.reshape((logits.shape[0]))
    self.node_count += 1

  def _reduce_mask(self, excite_mask):
    assert excite_mask.sum() > 0
    all_idx = numpy.arange(self.train_data.num_examples)
    excite_idx = all_idx[self.train_data.labels==1]
    rand_idx = numpy.random.randint(0, excite_idx.shape[0], 1)
    template_idx = excite_idx[rand_idx]
    i_mask = self.train_data.labels == 0
    e_mask, i_mask = self._update_masks(template_idx, excite_mask, i_mask)
    # If inhibit_mask is empty, remove associated excitatory instances
    if i_mask.sum() == 0:
      excite_mask[e_mask==1] = 0
    print("e/i mask: %d/%d\n" % (excite_mask.sum(), i_mask.sum()))
    return excite_mask

  def _sample_weights(self, excite_mask):
    inhibit_mask = self.train_data.labels==0
    assert numpy.sum(excite_mask) > 0
    assert numpy.sum(inhibit_mask) > 0
    if self.node_count == 0:
      excite_sum = float(numpy.sum(excite_mask))
      excite_weights = numpy.divide(excite_mask, excite_sum)
      inhibit_sum = float(numpy.sum(inhibit_mask))
      inhibit_weights = numpy.divide(inhibit_mask, inhibit_sum)
    else:
      # Weight samples by activation sum
      actv_sum = numpy.sum(self.node_actvs[0:self.node_count], axis=0)
      excite_max = numpy.max(actv_sum[excite_mask==1])
      excite_weights = numpy.subtract(excite_max, actv_sum)
      excite_sum = numpy.sum(excite_weights[excite_mask==1])
      # Deal with the case that excite_sum <= 0
      assert excite_sum != 0
      excite_weights = numpy.divide(excite_weights, excite_sum)
      excite_weights = numpy.multiply(excite_weights, excite_mask)
      inhibit_sum = numpy.sum(actv_sum[inhibit_mask==1])
      inhibit_weights = numpy.divide(actv_sum, inhibit_sum)
      inhibit_weights = numpy.multiply(inhibit_weights, inhibit_mask)
    return numpy.add(excite_weights, inhibit_weights)

  def _update_masks(self, excite_idx, excite_mask, inhibit_mask):
    all_samples = self.node_actvs[0:self.node_count]
    excite_template = all_samples[:, excite_idx]
    gt_template = numpy.greater_equal(all_samples, excite_template)
    lt_template = numpy.less_equal(all_samples, excite_template)
    gt_inhibit = numpy.sum(gt_template[:, inhibit_mask], axis=1)
    lt_inhibit = numpy.sum(lt_template[:, inhibit_mask], axis=1)
    binary_template = numpy.greater(lt_inhibit, gt_inhibit)
    binary_template = binary_template.reshape((binary_template.shape[0], 1))
    # Assign the values of all active values to the masks
    gt_mask = numpy.multiply(gt_template, binary_template)
    lt_mask = numpy.multiply(lt_template, ~binary_template)
    mask_sum = numpy.sum(numpy.logical_or(gt_mask, lt_mask), axis=0)
    mask_bool = numpy.greater_equal(mask_sum, mask_sum[excite_idx])
    excite_mask = numpy.logical_and(excite_mask, mask_bool)
    inhibit_mask = numpy.logical_and(inhibit_mask, mask_bool)
    return excite_mask, inhibit_mask

  def print_cluster(self, out_actvs, data_weights):
    excite_mask = self.train_data.labels==1
    excite_actvs = self.node_actvs[:, excite_mask]
    inhibit_actvs = self.node_actvs[:, ~excite_mask]
    # Print cluster layer stats
    delim_val = numpy.average(excite_actvs, axis=1)
    print("Excite stats: "+self.get_cluster_stats(excite_actvs, delim_val))
    print("Inhibit stats: "+self.get_cluster_stats(inhibit_actvs, delim_val))
    # Print output error stats
    excite_actvs = out_actvs[:, excite_mask]
    inhibit_actvs = out_actvs[:, ~excite_mask]
    delim_val = numpy.average(excite_actvs, axis=1)
    print("Excite out: "+self.get_cluster_stats(excite_actvs, delim_val))
    print("Inhibit out: "+self.get_cluster_stats(inhibit_actvs, delim_val))
    error_out = numpy.greater(out_actvs[0, :],out_actvs[1, :])
    excite_error = int(numpy.sum(error_out[excite_mask]))
    inhibit_error = int(numpy.sum(error_out[~excite_mask]))
    print("Output error, e/i: %d/%d\n" % (excite_error, inhibit_error))

  def get_cluster_stats(self, subset_actvs, delim_val):
    subset_n = int(subset_actvs.shape[1])
    # Calculate node activation sum
    subset_sum = str(numpy.sum(subset_actvs, axis=1))
    # Calculate average node activation
    subset_avg = str(numpy.average(subset_actvs, axis=1))
    # Calculate instances > average
    delim_val = delim_val.reshape((delim_val.shape[0], 1))
    subset_gtr = numpy.greater(subset_actvs, delim_val)
    subset_gtr = str(numpy.sum(subset_gtr, axis=1))
    # Generate formatted string
    cluster_str = "%d samples,\nsum: %s\navg: %s\ngtr: %s\n" % (
      subset_n, subset_sum, subset_avg, subset_gtr)
    return cluster_str