%{ 
extractor_subset.m
Clayton Kotulak
6/26/2016
 
Randomly select PARAMS.subset_n indices in the range [0, set_n]

Arguments: data_set matrix, data_mask matrix [same dimensions as data_set]
Returns: data_subset matrix, subset_mask matrix
%}
function [subset_a, subset_b] = extractor_subset(data_set, data_mask=[], subset_n=1)
	
    subset_a = [];
    subset_b = [];
    
    if numel(data_mask) != 0
        data_idx = 1:size(data_set, 1);
        data_idx = data_idx(data_mask == 1);
        rand_idx = randperm(size(data_idx, 2));
        rand_idx = data_idx(rand_idx);
    else
        rand_idx = randperm(size(data_set, 1));
    end

    subset_n = min([subset_n size(rand_idx, 2)]);
   
    % Select the first subset_n indices for subset_a
    subset_a = data_set(rand_idx(1:subset_n), :);
    subset_b = data_set(rand_idx(subset_n+1:end), :);
    
endfunction

