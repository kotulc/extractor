% extractor_xcost.m
% Clayton Kotulak
% 2/28/2015

% Calculate the cost of the parameter values theta with the input values X and target activation 
% values Y, with each sample weighted by the vector of influence values. theta is a n x 1 vector 
% of parameter values, X is a m x n matrix of training samples, and Y/influence are m x 1 vectors
function [J, gradient] = extractor_xcost(theta, thetaDim, X, Y, lambda)
	
	m = size(X, 1);
	
	% Reshape theta
	theta = reshape(theta, thetaDim(1), thetaDim(2));
	
	% Calculate the node activation values
	Z = X * theta;
	A = extractor_sigmoid(Z);
	
	% A is a m x output nodes matrix from a forward propagation, Y is a m x 1 vector of solutions. 
	J = -1/m * sum( sum( (Y .* log( A ) + (1 - Y) .* log(1 - A )) ) );
	
	% Add the regularization cost
	J = J + lambda/(2*m) * sum( sum(theta(2:end,:).^2) );
	
	delta = (A .- Y);
	%gradient = delta' * X;
	gradient = X' * delta;
	
	% Add the regularization gradient
	reg = [zeros(1,size(theta,2)); theta(2:end,:)];
	gradient = (1/m * gradient) + ((lambda/m) * reg);
	gradient = gradient(:);

end

