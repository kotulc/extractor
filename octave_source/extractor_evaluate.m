% extractor_evaluate.m
% Clayton Kotulak
% 1/10/2016

%{ 
Generate two output layers, one trained directly on the values of the sample
data, the other trained on the activations of the solution. Display metrics
appropriate to evaluate the accuracy of each.

Arguments: weight matrix, data struct
Returns: error struct
%}
function extractor_evaluate(sample_data, PARAMS)
    
    solved_flag = 0;
    batch_n = 50;
    
    excite_n = ceil(size(sample_data.excite_actvs, 1) .* 0.80);
    inhibit_n = ceil(size(sample_data.inhibit_actvs, 1) .* 0.80);
    
    [e_sub e_rem] = extractor_subset(sample_data.excite_actvs, [], excite_n);
    [i_sub i_rem] = extractor_subset(sample_data.inhibit_actvs, [], inhibit_n);
    
    fprintf("Generating evaluation nodes...");
    % Optimize the node weights
    instance_labels = [ones(batch_n, 1); zeros(batch_n, 1)];
            
    % initialize random weight values for node
    excite_weights = unifrnd(-1, 1, [size(e_sub, 2)+1 1]);
    inhibit_weights = unifrnd(-1, 1, [size(i_sub, 2)+1 1]);
    alpha = [(ones(batch_n, 1) ./ batch_n); ...
            (ones(batch_n, 1) ./ batch_n)];
            
    for i=1:2000,
        excite_idx = randint(batch_n, 1, [1 size(e_sub, 1)]);
        inhibit_idx = randint(batch_n, 1, [1 size(i_sub, 1)]);
        instances = [e_sub(excite_idx, :); i_sub(inhibit_idx, :)];
        
        excite_weights = extractor_xopt(...
                excite_weights, instances, instance_labels, alpha, PARAMS);
        inhibit_weights = extractor_xopt(...
                inhibit_weights, instances, ~instance_labels, alpha, PARAMS);
                
        %alpha = alpha .* 0.98;
        %PARAMS.lambda = PARAMS.lambda * 0.98;
    end 
    
    % Calculate the error of both nodes and display results
    fprintf("evaluating training instances...");
    
    excite_eactvs = extractor_fprop({excite_weights}, e_sub);
    excite_iactvs = extractor_fprop({excite_weights}, i_sub);
    
    inhibit_eactvs = extractor_fprop({inhibit_weights}, e_sub);
    inhibit_iactvs = extractor_fprop({inhibit_weights}, i_sub);
    
    e_actvs = excite_eactvs > inhibit_eactvs;
    i_actvs = excite_iactvs < inhibit_iactvs;
            
    e_count = size(e_sub, 1) - sum(e_actvs);
    i_count = size(i_sub, 1) - sum(i_actvs);
    
    error_prct = (e_count + i_count) / (size(e_sub, 1) + size(i_sub, 1));
    fprintf("\nM1 - Train e/i: %d/%d, errors: %d/%d, %f%%\n",...
            size(e_sub, 1), size(i_sub, 1), e_count, i_count, error_prct);
    
    
    excite_eactvs = extractor_fprop({excite_weights}, e_rem);
    excite_iactvs = extractor_fprop({excite_weights}, i_rem);
    
    inhibit_eactvs = extractor_fprop({inhibit_weights}, e_rem);
    inhibit_iactvs = extractor_fprop({inhibit_weights}, i_rem);
    
    e_actvs = excite_eactvs > inhibit_eactvs;
    i_actvs = excite_iactvs < inhibit_iactvs;
            
    e_count = size(e_rem, 1) - sum(e_actvs);
    i_count = size(i_rem, 1) - sum(i_actvs);
    
    error_prct = (e_count + i_count) / (size(e_rem, 1) + size(i_rem, 1));   
    fprintf("M1 - Eval e/i: %d/%d, errors: %d/%d, %f%%\n",...
            size(e_rem, 1), size(i_rem, 1), e_count, i_count, error_prct);
            
            
    % Optimize the node weights
    batch_n = 5000;
    PARAMS.optimization_iter = 500;
    instance_labels = [ones(batch_n, 1); zeros(batch_n, 1)];
            
    % initialize random weight values for node
    excite_weights = unifrnd(-1, 1, [size(e_sub, 2)+1 1]);
    inhibit_weights = unifrnd(-1, 1, [size(i_sub, 2)+1 1]);
    alpha = [(ones(batch_n, 1) ./ batch_n); ...
            (ones(batch_n, 1) ./ batch_n)];
            
    excite_idx = randint(batch_n, 1, [1 size(e_sub, 1)]);
    inhibit_idx = randint(batch_n, 1, [1 size(i_sub, 1)]);
    instances = [e_sub(excite_idx, :); i_sub(inhibit_idx, :)];
        
    excite_weights = extractor_xopt(...
                excite_weights, instances, instance_labels, alpha, PARAMS);
    inhibit_weights = extractor_xopt(...
            inhibit_weights, instances, ~instance_labels, alpha, PARAMS);
        
        
    excite_eactvs = extractor_fprop({excite_weights}, e_sub);
    excite_iactvs = extractor_fprop({excite_weights}, i_sub);
    
    inhibit_eactvs = extractor_fprop({inhibit_weights}, e_sub);
    inhibit_iactvs = extractor_fprop({inhibit_weights}, i_sub);
    
    e_actvs = excite_eactvs > inhibit_eactvs;
    i_actvs = excite_iactvs < inhibit_iactvs;
            
    e_count = size(e_sub, 1) - sum(e_actvs);
    i_count = size(i_sub, 1) - sum(i_actvs);
    
    error_prct = (e_count + i_count) / (size(e_sub, 1) + size(i_sub, 1));
    fprintf("M2 - Train e/i: %d/%d, errors: %d/%d, %f%%\n",...
            size(e_sub, 1), size(i_sub, 1), e_count, i_count, error_prct);
    
    
    excite_eactvs = extractor_fprop({excite_weights}, e_rem);
    excite_iactvs = extractor_fprop({excite_weights}, i_rem);
    
    inhibit_eactvs = extractor_fprop({inhibit_weights}, e_rem);
    inhibit_iactvs = extractor_fprop({inhibit_weights}, i_rem);
    
    e_actvs = excite_eactvs > inhibit_eactvs;
    i_actvs = excite_iactvs < inhibit_iactvs;
            
    e_count = size(e_rem, 1) - sum(e_actvs);
    i_count = size(i_rem, 1) - sum(i_actvs);
    
    error_prct = (e_count + i_count) / (size(e_rem, 1) + size(i_rem, 1));   
    fprintf("M2 - Eval e/i: %d/%d, errors: %d/%d, %f%%\n\n",...
            size(e_rem, 1), size(i_rem, 1), e_count, i_count, error_prct);
    fflush(stdout);
    
end

