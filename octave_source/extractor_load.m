% extractor_load.m
% Clayton Kotulak
% 1/27/2015

%{ 
If input data or feature maps have been saved from a previous extraction, load
them into their respective structures, else build and initialize the structures.

Returns: train data, val data, fmap struct
%}

function [train_data, val_data, test_data, nnet_layers] = extractor_load(params)

    nnet_layers = {};
	
	% Check to see if the input_data file exists
	input_data_flag = exist(params.input_data_file)==2;
	disp("Loading input data...");
	fflush(stdout);
	
	if (input_data_flag)
		% Load input data if the file has already been created
		input_data = load(params.input_data_file);

		train_data.inhibit_actvs = input_data.train_instances(...
				input_data.train_labels(:, 1)==0, :);
		train_data.excite_actvs = input_data.train_instances(...
				input_data.train_labels(:, 1)==1, :);

		val_data.inhibit_actvs = input_data.val_instances(...
				input_data.val_labels(:, 1)==0, :);
		val_data.excite_actvs = input_data.val_instances(...
				input_data.val_labels(:, 1)==1, :);
				
		test_data.inhibit_actvs = input_data.test_instances(...
				input_data.test_labels(:, 1)==0, :);
		test_data.excite_actvs = input_data.test_instances(...
				input_data.test_labels(:, 1)==1, :);
	else		
		% Load and partition the raw input data, then save all data
		image_data = loadMNISTImages('train-images.idx3-ubyte')';
		image_labels = loadMNISTLabels('train-labels.idx1-ubyte');
		
		% Not necessary for this data, already normalized.
		% image_data = extractor_normalize(image_data);
	  
		% Split the data into two classes, target and null (all remaining) 
		train_data.inhibit_actvs = ...
				image_data(image_labels!=params.target_label, :);
		train_data.excite_actvs = ...
				image_data(image_labels==params.target_label, :);
		
		% Randomly partition the training set into train and validation sets
        subset_n = ceil(size(train_data.inhibit_actvs, 1) .* params.val_percent);
		[inhibit_val, inhibit_train] = extractor_subset(...
                train_data.inhibit_actvs, [], subset_n);
                
        subset_n = ceil(size(train_data.excite_actvs, 1) .* params.val_percent);
		[excite_val, excite_train] = extractor_subset(...
                train_data.excite_actvs, [], subset_n);
                
         val_data.inhibit_actvs = inhibit_val;
         val_data.excite_actvs = excite_val;
         train_data.inhibit_actvs = inhibit_train;
         train_data.excite_actvs = excite_train;
		
		image_data = loadMNISTImages('t10k-images.idx3-ubyte')';
		image_labels = loadMNISTLabels('t10k-labels.idx1-ubyte');
		
		test_data.inhibit_actvs = ...
				image_data(image_labels!=params.target_label, :);
		test_data.excite_actvs = ...
				image_data(image_labels==params.target_label, :);

		extractor_save(train_data, val_data, test_data, params.input_data_file);
				
		disp("Raw image and label data partitioned.");
	end
	
    disp("Input data loaded.");
	fflush(stdout);
    
endfunction

