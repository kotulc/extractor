%{ 
extractor_layer.m
Clayton Kotulak
7/6/2016
 

Arguments: 
Returns: 
%}

function [layer_weights] = extractor_layer(train_data, max_nodes, PARAMS)
    
    layer_weights = [];

    % Split the excitatory instances and generate node clusters
    excite_n = size(train_data.excite_actvs, 1);
    excite_cluster_n = ceil(excite_n / PARAMS.node_clusters);
    inhibit_mask = ones(size(train_data.inhibit_actvs, 1), 1);
    
    for i=1:PARAMS.node_clusters
        start_idx = (i-1) * excite_cluster_n + 1;
        end_idx = min([(start_idx + excite_cluster_n - 1) excite_n]);
        excite_mask = zeros(excite_n, 1);
        excite_mask(start_idx:end_idx) = 1;
        
        cluster_weights = extractor_cluster(...
                train_data, [], [], excite_mask, inhibit_mask, 1, PARAMS);
        layer_weights = [layer_weights cluster_weights];
        disp("");
    end

    disp("Layer encoding final.\n");
    extractor_lstats(train_data, layer_weights, false);
    fflush(stdout);
    
    fprintf("\n\nTesting baseline activations... %d nodes\n",...
            size(train_data.excite_actvs, 2));
    extractor_evaluate(train_data, PARAMS);
    
    fprintf("\nTesting new layer nodes... %d nodes\n", size(layer_weights, 2));
    layer_data.excite_actvs = extractor_fprop(...
            {layer_weights}, train_data.excite_actvs);
    layer_data.inhibit_actvs = extractor_fprop(...
            {layer_weights}, train_data.inhibit_actvs);
    extractor_evaluate(layer_data, PARAMS);

endfunction

