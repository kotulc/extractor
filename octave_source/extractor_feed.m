%{ 
extractor_feed.m
Clayton Kotulak
9/18/2016



Arguments: 
Returns: 
%}

function [instances, labels, alpha] = extractor_feed(...
        train_data, instance_n, excite_idx, excite_itr, inhibit_idx=[]);
    
    %excite_batch = randint(instance_n, 1, [1 size(excite_idx, 2)]);
    %excite_instances = train_data.excite_actvs(excite_idx(excite_batch), :);
    
    if 0
        if instance_n > size(excite_idx, 1)
            extend_n = ceil(instance_n/size(excite_idx, 1));
            excite_idx = repmat(excite_idx, extend_n, 1);
        end
        
        end_idx = excite_itr + instance_n - 1;
        if end_idx > size(excite_idx, 1)
            excite_idx = repmat(excite_idx, 2, 1);
        end
        
        excite_batch = excite_idx(excite_itr:end_idx);
        excite_instances = train_data.excite_actvs(excite_batch, :);
    else
        excite_batch = randint(instance_n, 1, [1 size(excite_idx, 2)]);
        excite_instances = train_data.excite_actvs(...
                excite_idx(excite_batch), :);
    end
    
    if numel(inhibit_idx) != 0
        inhibit_batch = randint(instance_n, 1, [1 size(inhibit_idx, 2)]);
        inhibit_instances = train_data.inhibit_actvs(...
                inhibit_idx(inhibit_batch), :);
    else
        inhibit_batch = randint(...
                instance_n, 1, [1 size(train_data.inhibit_actvs, 2)]);
        inhibit_instances = train_data.inhibit_actvs(inhibit_batch, :);
    end

    instances = [inhibit_instances; excite_instances];
    labels = [zeros(instance_n, 1); ones(instance_n, 1)];
    alpha = ones(instance_n*2, 1) ./ instance_n*2;

endfunction