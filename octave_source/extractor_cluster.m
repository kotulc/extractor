%{ 
extractor_cluster.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

% excite_actvs and inhibit_actvs contain all cluster weight activations
function [cluster_weights] = extractor_cluster(train_data, excite_actvs,... 
        inhibit_actvs, excite_mask, inhibit_mask, cluster_depth, PARAMS)
    
    cluster_weights = [];
    
    if (cluster_depth > PARAMS.max_cluster_depth)
        return;
    elseif numel(excite_actvs)==0
        % Generate the cluster root node 
        fprintf("root node -\n");    
        [cluster_weights, excite_actvs, inhibit_actvs] = extractor_node(...
                train_data, excite_mask, inhibit_mask, PARAMS);
        cluster_depth = cluster_depth + 1;
    else
        fprintf("child nodes (l/r) -\n"); 
    end
    
    % Split the excitatory instances by node activation
    excite_filtered = excite_actvs(excite_mask==1);
    excite_median = median(excite_filtered);
    
    lexcite_mask = (excite_actvs >= excite_median) .* excite_mask;
    linhibit_mask = (inhibit_actvs >= excite_median) .* inhibit_mask;
    %linhibit_mask = (inhibit_actvs >= excite_median);
    
    rexcite_mask = (excite_actvs < excite_median) .* excite_mask;
    rinhibit_mask = (inhibit_actvs < excite_median) .* inhibit_mask;
    %rinhibit_mask = (inhibit_actvs < excite_median);
    
    % Generate the left and right branch nodes
    lbranch_flag = (sum(lexcite_mask) != 0) && (sum(linhibit_mask) != 0);
    if lbranch_flag
        [lnode_weights, le_actvs, li_actvs] = extractor_node(...
                train_data, lexcite_mask, linhibit_mask, PARAMS);
        # Take the next recursive step in the tree
        [lbranch_weights] = extractor_cluster(train_data, le_actvs,...
                li_actvs, lexcite_mask, linhibit_mask, cluster_depth+1, PARAMS);
        cluster_weights = [cluster_weights lnode_weights lbranch_weights];
    end
    
    rbranch_flag = (sum(rexcite_mask) != 0) && (sum(rinhibit_mask) != 0);
    if rbranch_flag
        [rnode_weights, re_actvs, ri_actvs] = extractor_node(...
                train_data, rexcite_mask, rinhibit_mask, PARAMS);
        # Take the next recursive step in the tree
        [rbranch_weights] = extractor_cluster(train_data, re_actvs,...
                ri_actvs, rexcite_mask, rinhibit_mask, cluster_depth+1, PARAMS);
        cluster_weights = [cluster_weights rnode_weights rbranch_weights];
    end

endfunction

