% extractor_main.m
% Clayton Kotulak
% 3/16/2015

%{ 
Driver function for extractor v1.2

MNIST data files may be downloaded here:
http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz

The driver function for the convolutional neural network feature extractor.
params is a global variable used to simplify function calls. See
extractor_params.m file for details concerning parameter definitions.
%}

function extractor_main()

    params = load('extractor_params.m');
    warning ('off', 'Octave:broadcast'); 
    #pkg load communications;

    % Load saved data and features
    [train_data val_data test_data nnet_layers] = extractor_load(params);

    % Uncomment below to display a subset of the MNIST data
    %displayMNISTSample(train_data.excite_actvs);
    %keyboard();
    %displayMNISTSample(train_data.inhibit_actvs);
    %keyboard();

    % Propagate the training data through the current network
    train_data.excite_actvs = extractor_fprop(...
            nnet_layers, train_data.excite_actvs);
    train_data.inhibit_actvs = extractor_fprop(...
            nnet_layers, train_data.inhibit_actvs);

    % Generate up to layer_max nnet layers
    [nnet_weights return_code] = extractor_nnet(train_data, params);
    keyboard();
    fflush(stdout);
    
    % Provide the option to overwrite existing layer data with the recently 
    % generated layer solution data
    q_string = cstrcat("\n\nWould you like to overwrite the data in ",...
            params.solution_data_file, "? (y/n): ");
    answer = input(q_string, "s");

    % Save feature maps and activations for later use
    if (strcmp(answer, "y") || strcmp(answer, "Y"))
        % Save weights from layer encoding
        save("-v6", "-z", params.solution_data_file, "solution");
            
        disp("Solution weight values saved.\n");
    else
        disp("Solution weight values discarded.\n");
    end	
  
endfunction

