%{ 
extractor_cluster.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

% excite_actvs and inhibit_actvs contain all cluster weight activations
function [cluster_weights] = extractor_cluster(train_data, cluster_mask, PARAMS)
    
    % Generate the cluster root node 
fprintf("root node -\n");    
    [node_weights, excite_actvs, inhibit_actvs] = extractor_node(...
            train_data, cluster_mask, [], PARAMS);
            
    cluster_weights = node_weights;
    e_mask = cluster_mask;
    i_mask = ones(size(train_data.inhibit_actvs, 1), 1);

    for i=2:PARAMS.max_cluster_depth - 1
        % Calculate the excite mask for this level
        e_actvs = excite_actvs(cluster_mask==1, end);
        e_avg = sum(e_actvs) / size(e_actvs, 1);
        e_mask = (excite_actvs(:, end) >= e_avg) .* cluster_mask;

        % Calculate the inhibit mask for this level
        i_sum = sum(inhibit_actvs(:, end), 2);
        i_avg = sum(i_sum) / size(i_sum, 1);
        %i_mask = i_sum >= i_avg;
        i_mask = i_sum >= e_avg;
        fprintf("\n");
        fprintf("reinforcement node - e_mask: %d, i_mask: %d\n",...
                sum(e_mask), sum(i_mask));
                
        if sum(i_mask) != 0
            % Generate the reinforcement leaf node
            [node_weights, e_actvs, i_actvs] = extractor_node(...
                    train_data, e_mask, i_mask, PARAMS);
            cluster_weights = [cluster_weights node_weights];
            inhibit_actvs = [inhibit_actvs i_actvs];
            excite_actvs = [excite_actvs e_actvs];
        end
        
        % Generate against remaining excitatory instances
        cluster_mask = ~e_mask .* cluster_mask;
        
        if sum(cluster_mask) == 0
            break;
        end    
         
        fprintf("\n");
        fprintf("branch node - cluster_mask: %d, i_mask: %d\n",...
                sum(cluster_mask), sum(i_mask));
                
        % Generate new branch node
        [node_weights, e_actvs, i_actvs] = extractor_node(...
                train_data, cluster_mask, [], PARAMS);
        cluster_weights = [cluster_weights node_weights];
        inhibit_actvs = [inhibit_actvs i_actvs];
        excite_actvs = [excite_actvs e_actvs];

        if sum(cluster_mask) <= 6
            break;
        end      
    end
    
    % Calculate the excite mask for this level
    e_actvs = excite_actvs(cluster_mask==1, end);
    e_avg = sum(e_actvs) / size(e_actvs, 1);
    e_mask = (excite_actvs(:, end) >= e_avg) .* cluster_mask;
        
    i_sum = sum(inhibit_actvs(:, end), 2);
    i_avg = sum(i_sum) / size(i_sum, 1);
    %i_mask = i_sum >= i_avg;
    i_mask = i_sum >= e_avg;
        
    if sum(i_mask) != 0
        % Generate the reinforcement leaf node
        [node_weights, e_actvs, i_actvs] = extractor_node(...
                train_data, e_mask, i_mask, PARAMS);
        cluster_weights = [cluster_weights node_weights];
    end

endfunction

