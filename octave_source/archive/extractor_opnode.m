%{ 
extractor_opnode.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

function [node_weights, excite_actvs, inhibit_actvs] = extractor_opnode(...
        train_data, excite_mask, inhibit_mask, PARAMS)
        
    % initialize random weight values for node
	node_weights = unifrnd(-1, 1, [size(train_data.excite_actvs, 2)+1 1]);
        
    excite_idx = (1:size(excite_mask, 1))(excite_mask == 1);
    inhibit_idx = (1:size(train_data.inhibit_actvs, 1))(inhibit_mask == 1);
    
    % Collect template_n inhibitory and excitatory templates
    for i=1:PARAMS.batch_n
        % Get the batch of training instances
        [batch_inst, batch_labels, batch_alpha] = extractor_feed(...
                train_data, PARAMS.excite_n, excite_idx, inhibit_idx);
        
        % Run a single optimization batch       
        node_weights = extractor_xopt(...
                node_weights, batch_inst, batch_labels, batch_alpha, PARAMS); 
    end
    
    % Calculate and display node metrics
    fprintf("New opnode stats:                             \n");
    [excite_actvs, inhibit_actvs] = extractor_nstats(train_data, node_weights);
    extractor_nstats(train_data, node_weights, excite_idx, inhibit_idx);
    
endfunction

