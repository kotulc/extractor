## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_seed (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-07-08

function [templates] = extractor_seed(...
        train_data, train_mask, templates, batch_n, invert_labels)
    
    % Get a list of remaining indices from train_mask
    train_idx = 1:size(train_mask, 1);
    train_idx = train_idx(train_mask == 1);
    
    % Select random excitatory instance batch from available instances
    rand_idx = randint(batch_n, 1, [1 size(train_idx, 2)]);
    excite_idx = train_idx(rand_idx);
    
    if invert_labels
        excite_batch = train_data.inhibit_actvs(excite_idx, :); 
        
        inhibit_idx = randint(batch_n, 1, [1 size(train_data.excite_actvs, 1)]);  
        inhibit_batch = train_data.excite_actvs(inhibit_idx, :);    
    else
        excite_batch = train_data.excite_actvs(excite_idx, :); 
        
        inhibit_idx = randint(batch_n, 1, [1 size(train_data.inhibit_actvs, 1)]);  
        inhibit_batch = train_data.inhibit_actvs(inhibit_idx, :);  
    end
    
    [excite_scores, inhibit_scores] = extractor_icompare(...
            excite_batch, inhibit_batch);
    %if 1    
    if numel(templates.excite_templates) == 0
        [val, excite_idx] =  max(excite_scores);
    else
        template_scores = extractor_ccompare(...
                templates.excite_templates, excite_batch);
        %keyboard();
        [val, excite_idx] = max(template_scores);
    end
        
    templates.excite_templates = [...
            templates.excite_templates; excite_batch(excite_idx, :)];
   
    %[val, inhibit_idx] = min(inhibit_scores); % Random is better
    inhibit_idx = randint(1, 1, [1 size(inhibit_batch, 1)]);
    
    templates.inhibit_templates = [...
            templates.inhibit_templates; inhibit_batch(inhibit_idx, :)];

endfunction
