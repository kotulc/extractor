## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_lstats(@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [retval] = extractor_lstats(train_data, layer_weights, invert_labels=0)

    e_actvs = extractor_fprop({layer_weights}, train_data.excite_actvs);
    e_actvs = sum(e_actvs.^2, 2);
    e_max = max(e_actvs);
    e_sum = sum(e_actvs);
    i_actvs = extractor_fprop({layer_weights}, train_data.inhibit_actvs);
    i_actvs = sum(i_actvs.^2, 2);
    i_max = max(i_actvs);
    i_sum = sum(i_actvs);
    e_avg = e_sum / size(e_actvs, 1);
    i_avg = i_sum / size(i_actvs, 1);
    e_max_i = sum(e_actvs > max(i_actvs));
    
    if invert_labels
        i_over_e = sum(i_actvs > e_avg);
        fprintf("l_stats  e/i actv_avg: %d/%d, actv_max: %d/%d, e_grtr: %d\n", ...
                i_avg, e_avg, i_max, e_max, i_over_e);
    else
        e_over_i = sum(e_actvs > i_avg);
        fprintf("l_stats  e/i actv_avg: %d/%d, actv_max: %d/%d, e_grtr: %d\n", ...
                e_avg, i_avg, e_max, i_max, e_over_i);
    end
    
endfunction
