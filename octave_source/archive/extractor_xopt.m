% extractor_xopt.m
% Clayton Kotulak
% 12/07/2014

%{ 
Utilize fmincg to optimize the weights matrix theta with respect to the 
included input and label values who's influence is weighted by alpha. Cost
is calculated with cross-entropy loss.

Arguments: weights matrix, input matrix, label matrix, alpha vector
Returns: weights matrix
%}
function theta = extractor_xopt(theta, X, Y, alpha, params)

	opt_iter = params.optimization_iter;
	lambda = params.lambda;
	
	% Create the training optimization option set
	train_opt = optimset('MaxIter', opt_iter);
	theta_dim = [size(theta,1) size(theta,2)];
	theta = theta(:);
	cost_params = [];
	
	% Add the bias value to the input 
	X = [ones(size(X,1),1) X];
	
	% Create costFunct for minimization, which takes a single parameter, 
	% cost_params, the vector theta
	costFunct = @(cost_params) extractor_xcost(cost_params,...
			theta_dim, X, Y, alpha, lambda);
	
	% Get the initial cost of theta
	[cost, gradient] = extractor_xcost(theta,...
			theta_dim, X, Y, alpha, lambda);
	
	if (1)
		fprintf('Initial cost of theta: %f\r',cost);
	end
	
	% Utilize fmincg to minimize the node weight parameters theta
	warning('off', 'Octave:possible-matlab-short-circuit-operator');
    fprintf('Size of theta: %d\r', size(theta));
	[theta, train_cost] = fmincg(costFunct, theta, train_opt);
	
	%if (numel(train_cost)>0)
    if (1)
		% Print the final cost of the optimized parameter matrix theta.
		fprintf('Final cost of theta: %f\r',...
				train_cost(end));
	end
	
	% Reshape theta
	theta = reshape(theta, theta_dim(1), theta_dim(2));
    
    fflush(stdout);
end

