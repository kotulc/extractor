## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_extract (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-07-08

function [train_mask, templates] = extractor_extract(layer_weights,...
        train_data, train_mask, templates, params, invert_labels=false)

    % Get a list of remaining indices from train_mask
    train_idx = 1:size(train_mask, 1);
    train_idx = train_idx(train_mask == 1);
    
    % Randomly shuffle train_data excite instances
    perm_idx = randperm(size(train_idx, 2));
    perm_idx = train_idx(perm_idx);

    start_idx = 1;
    end_idx = params.excite_n;
    end_idx = min([params.excite_n size(perm_idx, 2)]);
    
    % Iterate through excite batches until a non-separable batch is found.
    while start_idx <= size(perm_idx, 2)       
        % Swap the activation classes if invert_labels is true
        if invert_labels
            excite_batch = train_data.inhibit_actvs(...
                    perm_idx(start_idx:end_idx), :);
            
            idx_range = [1 size(train_data.excite_actvs, 1)];
            inhibit_idx = randint(params.inhibit_n, 1, idx_range);
            inhibit_batch = train_data.excite_actvs(inhibit_idx, :);
        else
            excite_batch = train_data.excite_actvs(...
                    perm_idx(start_idx:end_idx), :);
                    
            idx_range = [1 size(train_data.inhibit_actvs, 1)];
            inhibit_idx = randint(params.inhibit_n, 1, idx_range);
            inhibit_batch = train_data.inhibit_actvs(inhibit_idx, :);
        end
        
        [excite_mask, excite_tmplt, inhibit_tmplt] = extractor_separate(...
                layer_weights, excite_batch, inhibit_batch, params.score_min);
        %fprintf("sum/min %d %d\r", sum(excite_mask), min(excite_scores));
       
        % update the training mask if the excite mask contains null values
        train_mask(perm_idx(start_idx:end_idx)) = ...
                train_mask(perm_idx(start_idx:end_idx)) .* excite_mask;
        
        if sum(excite_mask) > 0
            if numel(templates.excite_templates) > 0
                template_scores = extractor_ccompare(...
                    templates.excite_templates, excite_batch);
                [score, excite_idx] = max(template_scores);
                excite_tmplt = excite_batch(excite_idx, :);
                train_mask(excite_idx) = 0;
            end
            templates.excite_templates = [...
                    templates.excite_templates; excite_tmplt];
            templates.inhibit_templates = [...
                    templates.inhibit_templates; inhibit_tmplt];
            return;
        end
        
        start_idx = end_idx + 1;
        end_idx = min([(start_idx + params.excite_n - 1) size(perm_idx, 2)]);

    end     

endfunction
