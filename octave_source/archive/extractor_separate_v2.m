%{ 
extractor_separate.m
Clayton Kotulak
8/23/2016



Arguments: 
Returns: 
%}

function [batch_inst, batch_labels, batch_alpha] = extractor_separate(...
                    layer_weights, excite_batch, inhibit_batch, score_min);

    batch_inst = [];
    batch_labels = [];
    batch_alpha = [];
    
    if numel(layer_weights) == 0
        instance_n = min([size(excite_batch, 1), size(inhibit_batch, 1)]);
        batch_inst = [excite_batch(1:instance_n, :); ...
                inhibit_batch(1:instance_n, :)];
        batch_labels = [ones(instance_n, 1); zeros(instance_n, 1)];
        batch_alpha = ones(instance_n*2, 1) ./ instance_n;
        return;
    end
    
    % calculate inhibitory and excitatory layer activation
    excite_actvs = extractor_fprop({layer_weights}, excite_batch);
    inhibit_actvs = extractor_fprop({layer_weights}, inhibit_batch);

    delta_scores = [];
    inhibit_indices = [];
    
    for i=1:size(excite_actvs, 1),
        actv_scores = sum((excite_actvs(i, :) .- inhibit_actvs).^2, 2);
        
        [delta_score, inhibit_idx] = min(actv_scores);
        delta_scores = [delta_scores; delta_score];
        inhibit_indices = [inhibit_indices; inhibit_idx];
    end   

    delta_mask = score_min >= delta_scores;
    
    if sum(delta_mask) > 0
        excite_inst = excite_batch(delta_mask, :);
        inhibit_idx = inhibit_indices(delta_mask);
        inhibit_inst = inhibit_batch(inhibit_idx, :);
        
        batch_inst = [excite_inst; inhibit_inst];
        batch_labels = [ones(size(excite_inst, 1), 1);...
                zeros(size(inhibit_inst, 1), 1)];
        
        excite_alpha = (score_min .- delta_scores(delta_mask)) ./ score_min;
        excite_alpha = excite_alpha / sum(excite_alpha); 
        inhibit_alpha = ones(size(inhibit_inst, 1), 1) / size(inhibit_inst, 1);
        batch_alpha = [excite_alpha; inhibit_alpha.*2]; %%% TESTING
    end

endfunction