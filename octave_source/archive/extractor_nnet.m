## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_nnet (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [nnet_weights return_code] = extractor_nnet (train_data, params)

    layer_idx = 1;
    return_code = 1;
    max_nodes = params.max_nodes;
    excite_n = size(train_data.excite_actvs, 1);
    inhibit_n = size(train_data.inhibit_actvs, 1)
    data_mask.excite_mask = ones(excite_n, 1);
    data_mask.inhibit_mask = ones(inhibit_n, 1);
    nnet_weights = {[]};
    
    layer_data.excite_actvs = train_data.excite_actvs;
    layer_data.inhibit_actvs = train_data.inhibit_actvs;
    
    while layer_idx <= params.max_layers
        % Generate new layer
        fprintf("\n\nGenerating layer %d nodes...\n", layer_idx);
        [layer_weights temp_mask] = extractor_layer(...
                layer_data, data_mask, max_nodes, params);
        nnet_weights{layer_idx} = [nnet_weights{layer_idx} layer_weights];
        keyboard();
        
        % If the layer already has a child layer...
        if layer_idx != size(nnet_weights, 2)
            % Initialize all weights connecting the two to zero
            disp("Filling child weights...\n");
            weight_n = size(nnet_weights{layer_idx + 1}, 2);
            new_weights = zeros(size(layer_weights, 2),  weight_n);
            nnet_weights{layer_idx+1} = [nnet_weights{layer_idx+1}; new_weights];
        end
        
        layer_flag = sum(temp_mask.excite_mask) + sum(temp_mask.inhibit_mask);
        
        % If layer fails to separate all instances, update parent layer
        if layer_flag > 0
            if layer_idx == 1
                % Layer does not have a parent layer, return with an error
                disp("Error: Root layer insufficient.\n");
                return;
            elseif layer_idx == 2
                % Parent layer is the first layer, set max_nodes accordingly 
                max_nodes = params.max_nodes - size(nnet_weights{1}, 2);
            else
                % Maximum layer nodes must be less than their parent layer
                max_nodes = size(nnet_weights{layer_idx-2}, 2) - ...
                        size(nnet_weights{layer_idx-1}, 2) - 1;
            end
            
            % If the last layer in the network is insufficient...
            keyboard();
            if layer_idx == size(nnet_weights, 2)
                % Update the working instance mask
                fprintf("Parent layer insufficient. n=%d\n", layer_flag);
                data_mask = temp_mask;
            end  
            
            % Return focus to the parent layer and resolve insufficiencies
            layer_idx = layer_idx - 1;
        else
            % Test layer performance
            solved_flag = extractor_evaluate(train_data, params);
            solved_flag = extractor_evaluate(train_data, params);
            solved_flag = extractor_evaluate(train_data, params);
            %solved_flag = (size(nnet_weights{layer_idx}, 2) == 2);
            if solved_flag
                % Instances separated with a single node, return
                return_code = 0;
                return;
            end
            
            % Prepare to generate the next layer
            data_mask.excite_mask = ones(excite_n, 1);
            data_mask.inhibit_mask = ones(inhibit_n, 1);
            max_nodes = size(nnet_weights{layer_idx}, 2);
            layer_idx = layer_idx + 1;
            nnet_weights{layer_idx} = [];
        end
        
        layer_data.excite_actvs = extractor_fprop(...
                nnet_weights(1:layer_idx-1), train_data.excite_actvs);
        layer_data.inhibit_actvs = extractor_fprop(...
                nnet_weights(1:layer_idx-1), train_data.inhibit_actvs);
                
        fflush(stdout);
    end

endfunction
