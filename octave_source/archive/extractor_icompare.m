%{ 
extractor_icompare.m
Clayton Kotulak
6/25/2016

Calculate the inner comparison between all instances contained in x 
where each instance belongs to class 0 or class 1. Instances in x are 
assumed to be normalized in the range [0,1] and the labels are assumed 
to be binary. x should contain at least one instance from both classes.

Arguments: instances x, labels y
Returns: score matrix
%}

function [x1_scores, x2_scores] = extractor_icompare(x1, x2)

    x = [x1; x2];
    y = [ones(size(x1, 1), 1); zeros(size(x2, 1), 1)];
        
    % Count the number of class 0 instances, set to 1 if none 
    y_count = size(x2, 1);
    y_count = y_count + (y_count == 0);
    
    % Calculate the multiplier for class 0 to ensure a balance between
    % class 0 and class 1 instances
    y_mult = -1 * sum(y==1) / y_count;
    
    % Extend each instance with its uniary difference
    x = [x (1 .- x)];
    
    % Calculate the squared feature-wise instance similarity
    feature_sums = sum([x1 (1 .- x1)]);
    x = x .* feature_sums;
    mat_prod = x*x';
   
    % Set all class 0 instances to negative values
    mat_prod(:, y==0) = mat_prod(:, y==0) .* y_mult;
    instance_scores = sum(mat_prod, 2);
    
    x1_scores = instance_scores(y==1);
    x2_scores = instance_scores(y==0);

endfunction