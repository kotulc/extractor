## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_batch(@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-07-08

function [templates, batch_flag] = extractor_batch(...
        layer_weights, templates, excite_batch, inhibit_batch)
    
    batch_flag = 0;
    
    % calculate inhibitory and excitatory layer activation
    excite_actvs = extractor_fprop({layer_weights}, excite_batch);
    inhibit_actvs = extractor_fprop({layer_weights}, inhibit_batch);
    
    % Compare excite and inhibit layer activations
    %[excite_scores, inhibit_scores] = ...
    %       extractor_icompare(excite_actvs, inhibit_actvs);
    [excite_val, excite_idx] = min(excite_scores);
    
    if size(layer_weights, 2) == 10
        keyboard();
    end
    
    % non-separable batches have at least one misplaced excite sign
    if excite_val <= 0
        batch_flag = 1;
        
        if numel(templates.excite_templates) > 0
            template_scores = extractor_ccompare(...
                    templates.excite_templates, excite_batch);
            [excite_val, excite_idx] = max(template_scores);
        end
        
        templates.excite_templates = [...
                templates.excite_templates; excite_batch(excite_idx, :)];
        
        [inhibit_val, inhibit_idx] = max(inhibit_scores);
        %templates.inhibit_templates = [templates.inhibit_templates;...
                %inhibit_batch(inhibit_idx, :)];
        templates.inhibit_templates = [templates.inhibit_templates;...
                inhibit_batch(randint(1, 1, [1 size(inhibit_batch, 1)]), :)];
    end     

endfunction
