%{ 
extractor_separate.m
Clayton Kotulak
8/23/2016



Arguments: 
Returns: 
%}

function [delta_mask, excite_tmplt, inhibit_tmplt] = extractor_separate(...
        layer_weights, excite_batch, inhibit_batch, score_min)

    % calculate inhibitory and excitatory layer activation
    excite_actvs = extractor_fprop({layer_weights}, excite_batch);
    inhibit_actvs = extractor_fprop({layer_weights}, inhibit_batch);
        
    %Test vs vector method
    if 1
        delta_scores = [];
        inhibit_indices = [];
        
        for i=1:size(excite_actvs, 1),
            actv_scores = sum((excite_actvs(i, :) .- inhibit_actvs).^2, 2);
            
            [delta_score, inhibit_idx] = min(actv_scores);
            delta_scores = [delta_scores; delta_score];
            inhibit_indices = [inhibit_indices; inhibit_idx];
        end   
    else 
        feature_n = size(excite_actvs, 2);
        excite_n = size(excite_actvs, 1);
        inhibit_n = size(inhibit_actvs, 1);
        
        % flatten inhibit_actvs to a vector
        inhibit_actvs = inhibit_actvs'(:);

        % extend excite_actvs to match inhibit_actvs dim
        excite_actvs = repmat(excite_actvs', inhibit_n, 1);
        
        % calculate the per-feature difference and reshape
        delta_actvs = (excite_actvs .- inhibit_actvs).^2;
        delta_actvs = reshape(delta_actvs, feature_n, excite_n*inhibit_n);
        delta_actvs = sum(delta_actvs, 1);
        delta_actvs = reshape(delta_actvs, inhibit_n, excite_n);
        
        % Calculate instance scores and mask
        [delta_scores, inhibit_indices] = min(delta_actvs)';
    end
    
    delta_mask = score_min >= delta_scores;
    excite_tmplt = [];
    inhibit_tmplt = [];
    
    if sum(delta_mask) > 0
        [excite_val, excite_idx] = min(delta_scores);
        excite_tmplt = excite_batch(excite_idx, :);
        delta_mask(excite_idx) = 0;
        
        inhibit_idx = inhibit_indices(excite_idx);
        inhibit_tmplt = inhibit_batch(inhibit_idx, :);
    end

endfunction