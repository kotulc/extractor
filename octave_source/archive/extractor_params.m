% extractor_params.m
% Clayton Kotulak
% 7/10/2016


%{ 
The name of the file used to store the solution data. If this file does not 
exist it will be created. If this file already exists, the user will be 
prompted to confirm the overwrite of its contents.

A string that forms a valid file name
See extractor_main
%}
# name: solution_data_file
# type: string
# elements: 1
# length: 17
solution_data.dat


%{ 
The name of the files used to store the input/output data partitioned into 
training, validation and test sets. If these files does not exist it will be 
created once the raw MNIST data has been partitioned. If this file exists, 
the system will load input data from this file instead of the default MNIST 
data files.

A string that forms a valid file name
See extractor_main, extractor_load
%}
# name: input_data_file
# type: string
# elements: 1
# length: 14
input_data.dat


# name: output_data_file
# type: string
# elements: 1
# length: 15
output_data.dat


%{ 
The name of the saved feature map data file. If this file does not exist it 
will be created.

A string that forms a valid file name
See extractor_main, extractor_load
%}
# name: feature_data_file
# type: string
# elements: 1
# length: 16
feature_data.dat


%{ 
The fraction of training instances to reserve for validation purposes.

A float value in the range (0,1)
See extractor_load
%}
# name: val_percent
# type: scalar
0.08


%{ 
The class label targeted for training against all other numeric classes.

An integer value in the range [0,class_label_n-1]
See extractor_load
%}
# name: target_label
# type: scalar
8


%{
An integer value in the range [1,m) 
See 
%}
# name: max_layers
# type: scalar
10


%{ 
An integer value in the range [1,m)
See 
%}
# name: max_nodes
# type: scalar
200


%{ 
An integer value in the range [1,m)
See 
%}
# name: drop_type
# type: scalar
1


%{ 
An integer value in the range [1,m)
See 
%}
# name: drop_rate
# type: scalar
0.00


%{ 
An integer value in the range [1,m)
See 
%}
# name: score_min
# type: scalar
0.75


%{ 
An integer value in the range [1,m)
See 
%}
# name: inhibit_n
# type: scalar
150


%{ 
An integer value in the range [1,m) 
See 
%}
# name: excite_n
# type: scalar
50


%{ 
An integer value in the range [1,m)
See 
%}
# name: template_n
# type: scalar
25


# name: precision_n
# type: scalar
5


%{ 
An integer value in the range [1,m)
See extractor_xopt
%}
# name: optimization_iter
# type: scalar
80


%{ 
An integer value in the range [1,m)
See extractor_xopt
%}
# name: lambda
# type: scalar
1

