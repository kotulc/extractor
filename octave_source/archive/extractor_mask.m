%{ 
extractor_mask.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

% excite_actvs and inhibit_actvs contain all cluster weight activations
function [excite_mask, cluster_mask, inhibit_mask] = extractor_mask(...
        excite_mask, cluster_mask, excite_actvs, inhibit_actvs) 
        
    % Determine if any instances may be eliminated - test with this not included
    inhibit_max = max(inhibit_actvs(:, end));
    inhibit_sum = max(sum(inhibit_actvs, 2));
    excite_sum = sum(excite_actvs, 2);
    excite_flag = (excite_sum <= inhibit_sum) .* (...
            excite_actvs(:, end) <= inhibit_max);

    % Update the cluster and layer mask
    excite_n = sum(excite_mask); 
    excite_mask = excite_mask .* excite_flag;
    fprintf("Excite mask delta: %d\n", excite_n - sum(excite_mask));
    
    % Can leave this part out to test and not require end if-case
    %cluster_mask = cluster_mask .* excite_flag;

    % Reduce the size of the cluster mask
    cluster_actvs = excite_actvs(cluster_mask == 1, end);
    excite_avg = sum(cluster_actvs) / size(cluster_actvs, 1);           
    cluster_mask = (excite_actvs(:, end) >= excite_avg) .* cluster_mask;

    cluster_count = sum(cluster_mask)
    inhibit_mask = inhibit_actvs(:, end) >= excite_avg;
    inhibit_count = sum(inhibit_mask)
     
endfunction

