%{ 
extractor_feed.m
Clayton Kotulak
9/18/2016



Arguments: 
Returns: 
%}

function [instances, labels, train_idx, start_idx, perm_count] =...
                extractor_feed(layer_weights, train_data,...
                train_idx, start_idx, perm_count, params);
    
    instances = [];
    labels = [];
    
    if numel(layer_weights) == 0
        excite_idx = randint(params.excite_n, 1,...
                [1 size(train_data.excite_actvs, 1)]);
        inhibit_idx = randint(params.excite_n, 1,...
                [1 size(train_data.inhibit_actvs, 1)]);
        instances = [train_data.excite_actvs(excite_idx, :);...
                train_data.inhibit_actvs(inhibit_idx, :)];
        labels = [ones(params.excite_n, 1); zeros(params.excite_n, 1)];
        return;
    end
    
    if start_idx == 0
        start_idx = 1;
        train_idx = randperm(size(train_data.excite_actvs, 1));
    end
    
    excite_n = numel(train_idx);
    inhibit_n = size(train_data.inhibit_actvs, 1);
    inhibit_range = [1 inhibit_n];
    
    % Iterate through excite batches until a non-separable batch is found.
    while perm_count <= params.precision_n       

        while start_idx <= excite_n   
            
            end_idx = start_idx + params.excite_n - 1;
            end_idx = min([end_idx excite_n]);

            excite_batch = train_data.excite_actvs(...
                    train_idx(start_idx:end_idx), :);
                    
            inhibit_idx = randint(params.inhibit_n, 1, inhibit_range);
            inhibit_batch = train_data.inhibit_actvs(inhibit_idx, :);
            
            [batch_inst, batch_labels] = extractor_separate(...
                    layer_weights, excite_batch, inhibit_batch, params);
            fprintf("Batch size: %d\n", size(batch_inst, 1));
         
            instances = [instances; batch_inst];
            labels = [labels; batch_labels];

            if sum(labels) >= params.excite_n
                return;
            end
            
            start_idx = end_idx + 1;
        end
        
        % Re-shuffle excite instances
        start_idx = 1;
        train_idx = randperm(size(train_data.excite_actvs, 1));
        perm_count = perm_count + 1;
    end 

endfunction