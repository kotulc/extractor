%{ 
extractor_node.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

function [node_weights, excite_actvs, inhibit_actvs] = extractor_node(...
        layer_weights, train_data, train_mask, invert, PARAMS)

    start_idx = 0;
    perm_count = 1;
    alpha_mod = 1;
    node_weights = [];
    train_idx = [];

    % initialize random weight values for node
    feature_n = size(train_data.excite_actvs, 2);
	node_weights = unifrnd(-1, 1, [feature_n+1 1]); 
    %window_mask = extractor_window(feature_n);
    
    % Collect template_n inhibitory and excitatory templates
    for i=1:PARAMS.batch_n
        
        [instances, labels, train_idx, start_idx, perm_count] =...
                extractor_feed(layer_weights, train_data, train_mask,...
                train_idx, start_idx, perm_count, PARAMS);

        if numel(instances)==0
            fprintf("All instances are separable.");
            break;
        end
        
        %instances = instances .* window_mask;
        %[instances] = extractor_dropout(instances, 0.25);
        %displayMNISTSample(instances);
        %keyboard();
        
        %alpha = alpha .* alpha_mod;
        %alpha_mode = alpha_mod * 0.99;
        %PARAMS.lambda = PARAMS.lambda * 0.99;
        alpha = ones(size(instances, 1), 1);
        alpha = alpha ./ size(alpha, 1);
        
        % Run a single optimization batch
        node_weights = extractor_xopt(...
                node_weights, instances, labels, alpha, PARAMS); 
    end
    
    if perm_count > PARAMS.precision_n
        node_weights = [];
        excite_actvs = [];
        inhibit_actvs = [];
        return;
    end
    
    excite_actvs = extractor_fprop({node_weights}, train_data.excite_actvs).^2;
    excite_sum = sum(excite_actvs);
    excite_n = size(excite_actvs, 1);
    
    inhibit_actvs = extractor_fprop({node_weights}, train_data.inhibit_actvs).^2;
    inhibit_sum = sum(inhibit_actvs);
    inhibit_n = size(inhibit_actvs, 1);
    
    if invert
        ratio = inhibit_sum / (excite_sum * (inhibit_n/excite_n));
    else
        ratio = excite_sum / (inhibit_sum * (excite_n/inhibit_n));
    end

    fprintf("n_stats  e/i sum: %d/%d, gtr_0.5: %d/%d  ratio: %d (%d/%d inst.)\n",...
                excite_sum, inhibit_sum, sum(excite_actvs > 0.5),...
                sum(inhibit_actvs > 0.5), ratio, excite_n, inhibit_n);  
    %keyboard();
    
endfunction

