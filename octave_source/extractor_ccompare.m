%{ 
extractor_ccompare.m
Clayton Kotulak
6/26/2016

Calculate the cross-comparison between all instances contained in x1 
and all instances contained in x2. Instances in both x1 and x2 are 
assumed to be normalized in the range [0,1].

Arguments: instances x1, instances x2
Returns: score matrix
%}

function instance_scores = extractor_ccompare(x1, x2)

    % Extend each instance with its uniary difference
    %x1 = [x1 (1 .- x1)];
    %x2 = [x2 (1 .- x2)];
    
    % Calculate the instance scores
    instance_scores = sum(x2*x1', 2);

endfunction