%{ 
extractor_separate.m
Clayton Kotulak
8/23/2016



Arguments: 
Returns: 
%}

function [batch_inst, batch_labels] = extractor_separate(...
        layer_weights, excite_batch, inhibit_batch, params);

    batch_inst = [];
    batch_labels = [];
    
    if numel(layer_weights) == 0
        disp("extractor_separate Error: Null layer weights.");
        return;
    end
    
    % calculate inhibitory and excitatory layer activation
    excite_actvs = extractor_fprop({layer_weights}, excite_batch);
    inhibit_actvs = extractor_fprop({layer_weights}, inhibit_batch);

    e_mask = [];
    i_scores = [];
    
    for i=1:size(excite_actvs, 1),
        e_inst = excite_actvs(i, :) ./ sum(excite_actvs(i, :));
        i_inst = inhibit_actvs ./ sum(inhibit_actvs, 2);
        e_score = sum(e_inst.^2, 2);
        i_score = sum(i_inst .* e_inst, 2);
        %e_score = sum(excite_actvs(i, :), 2);
        %i_score = sum(inhibit_actvs, 2);
        
        e_bools = e_score > (i_score + params.min_score);
        e_mask(i) = sum(e_bools) != size(e_bools, 1);
    end   
    
    if sum(e_mask) > 0
        excite_inst = excite_batch(e_mask==1, :);
        inhibit_inst = inhibit_batch(1:size(excite_inst, 1), :);
        
        batch_inst = [excite_inst; inhibit_inst];
        batch_labels = [ones(size(excite_inst, 1), 1);...
                zeros(size(inhibit_inst, 1), 1)];
    end

endfunction