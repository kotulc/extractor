%{ 
extractor_node.m
Clayton Kotulak
6/26/2016
 

Arguments: 
Returns: 
%}

function [node_weights, excite_actvs, inhibit_actvs] = extractor_node(...
        train_data, excite_mask, inhibit_mask, PARAMS)

    start_idx = 0;
    perm_count = 1;
    train_idx = [];
    train_flag = false;

    % generate the list of instance indices
    if numel(excite_mask) != 0
        excite_idx = (1:size(excite_mask, 1))(excite_mask==1);
    else
        excite_idx = 1:size(train_data.excite_actvs, 1);
    end
    
    if numel(inhibit_mask) != 0
        inhibit_idx = (1:size(inhibit_mask, 1))(inhibit_mask==1);
    else
        inhibit_idx = 1:size(train_data.inhibit_actvs, 1);
    end
    
    % initialize random weight values for node
    feature_n = size(train_data.excite_actvs, 2);
	node_weights = unifrnd(-1, 1, [feature_n+1 1]); 
    excite_itr = 1;
    
    % Collect template_n inhibitory and excitatory templates
    for i=1:PARAMS.batch_n     
        % Get the batch of training instances
        [batch_inst, batch_labels, batch_alpha] = extractor_feed(train_data,...
                PARAMS.excite_n, excite_idx, excite_itr, inhibit_idx);

        % Run a single optimization batch
        node_weights = extractor_xopt(...
                node_weights, batch_inst, batch_labels, batch_alpha, PARAMS); 
                
        excite_itr = excite_itr + PARAMS.excite_n;
        if excite_itr > size(excite_idx, 1)
            excite_itr = 1;
        end
    end
    
    % Calculate and display node metrics
    fprintf("New node stats:                             \n");
    [excite_actvs, inhibit_actvs] = extractor_nstats(train_data, node_weights);
    extractor_nstats(train_data, node_weights, excite_idx, inhibit_idx);
    
endfunction

