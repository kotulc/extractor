## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_nnet (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [nnet_weights return_code] = extractor_nnet(train_data, PARAMS)

    layer_idx = 1;
    return_code = 1;
    max_nodes = PARAMS.max_nodes;
    excite_n = size(train_data.excite_actvs, 1);
    inhibit_n = size(train_data.inhibit_actvs, 1);
    nnet_weights = {[]};
    
    layer_data.excite_actvs = train_data.excite_actvs;
    layer_data.inhibit_actvs = train_data.inhibit_actvs;
    
    while layer_idx <= PARAMS.max_layers
        % Generate new layer
        fprintf("\n\nGenerating layer %d nodes...\n", layer_idx);
        [layer_weights] = extractor_layer(layer_data, max_nodes, PARAMS);
        nnet_weights{layer_idx} = [nnet_weights{layer_idx} layer_weights];
 
        % Increase the number of layer training instances proportional to 
        % the decrease in input features (keep cost linear)
        train_mod = size(layer_data.excite_actvs, 2)/size(layer_weights, 2);
        PARAMS.excite_n = round(PARAMS.excite_n*train_mod);
        PARAMS.inhibit_n = round(PARAMS.excite_n*train_mod);
        keyboard();
        
        % Prepare to generate the next layer
        %max_nodes = size(nnet_weights{layer_idx}, 2);
        
        layer_data.excite_actvs = extractor_fprop(...
                {layer_weights}, layer_data.excite_actvs);
        layer_data.inhibit_actvs = extractor_fprop(...
                {layer_weights}, layer_data.inhibit_actvs);
                
        layer_idx = layer_idx + 1;
        nnet_weights{layer_idx} = [];
    end

endfunction
