## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_localdrop(@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [instances] = extractor_localdrop(instances, keep_rate)
    
    feature_n = size(instances, 2);
    keep_n = ceil(feature_n * keep_rate);
    
    focus_idx = randint(1, 1, [1 feature_n]);
    focus_idx = max([1 focus_idx-keep_n]);

    instances(:, 1:focus_idx-1) = 0;
    instances(:, focus_idx+keep_n:end) = 0;
    
endfunction
