% extractor_save.m
% Clayton Kotulak
% 1/12/2015

%{ 
Save the training, validation and test data sets in a format compatible with
the TensorFlow evaluation utilities 

Arguments: data struct, data struct, data struct, file string, weight matrix
%}
function extractor_save(train_data, val_data, test_data, file_name, solution=[])
	
	disp("Saving data structures...");
	
	if (numel(solution)>0)
		% Calculate the layer (solution) activation values
		train_data.inhibit_actvs = extractor_fprop(...
				{solution}, train_data.inhibit_actvs);
		train_data.excite_actvs = extractor_fprop(...
				{solution}, train_data.excite_actvs);
				
		val_data.inhibit_actvs = extractor_fprop(...
				{solution}, val_data.inhibit_actvs);
		val_data.excite_actvs = extractor_fprop(...
				{solution}, val_data.excite_actvs);
				
		test_data.inhibit_actvs = extractor_fprop(...
				{solution}, test_data.inhibit_actvs);
		test_data.excite_actvs = extractor_fprop(...
				{solution}, test_data.excite_actvs);
	end
	
	train_inhibit_n = size(train_data.inhibit_actvs, 1);
	train_excite_n = size(train_data.excite_actvs, 1);
	val_inhibit_n = size(val_data.inhibit_actvs, 1);
	val_excite_n = size(val_data.excite_actvs, 1);
	test_inhibit_n = size(test_data.inhibit_actvs, 1);
	test_excite_n = size(test_data.excite_actvs, 1);
	
	% Format the train data for export to evalnets tensor flow test scripts
	train_instances = [train_data.inhibit_actvs; train_data.excite_actvs];
	train_labels = [zeros(train_inhibit_n, 1) ones(train_inhibit_n, 1);...
			ones(train_excite_n, 1) zeros(train_excite_n, 1)];
			
	val_instances = [val_data.inhibit_actvs; val_data.excite_actvs];
	val_labels = [zeros(val_inhibit_n, 1) ones(val_inhibit_n, 1);...
			ones(val_excite_n, 1) zeros(val_excite_n, 1)];
			
	test_instances = [test_data.inhibit_actvs; test_data.excite_actvs];
	test_labels = [zeros(test_inhibit_n, 1) ones(test_inhibit_n, 1);...
			ones(test_excite_n, 1) zeros(test_excite_n, 1)];
			
	% Save net partitioned input instances and corresponding labels
	save("-v6", file_name,...
			"train_instances", "train_labels",...
			"val_instances", "val_labels",...
			"test_instances", "test_labels");
	
	disp("Data structures saved.");

end

