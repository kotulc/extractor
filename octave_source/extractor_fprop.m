% extractor_fprop.m 
% Clayton Kotulak 
% 7/29/2014

%{ 
Given the network weight cell w and feature matrix x (including bias x), 
propagate the signal through the network and return the activations of the last 
layer as the matrix 'x'

Arguments: weights cell, input matrix
Returns: activation matrix
%}
function output_actvs = extractor_fprop(w, x)
    
    if numel(w) == 0
        output_actvs = x;
        return;
    end
    
    output_actvs = [];
    max_instances = 5000;
    batch_n = ceil(size(x, 1)/max_instances);
    
    for i=1:batch_n,
        start_idx = (i-1)*max_instances + 1;
        end_idx = min([(start_idx+max_instances-1), size(x, 1)]);           
        actvs = x(start_idx:end_idx, :);
        
        for j=1:size(w, 2),
            % Add the bias activation to the x matrix. size is now m x nodes+1
            actvs = [ones(size(actvs, 1), 1) actvs];

            % w{i} is the nodes x w weight layer [w==f+1]. z is a m x nodes 
            % weighted activation matrix
            z = actvs * (w{j});
            actvs = extractor_sigmoid(z);
        end
        
        output_actvs = [output_actvs; actvs];
	end
    
end
