## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_window(@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [window_mask] = extractor_window(feature_n, window_dim=0)
    
    inst_dim = floor(sqrt(feature_n));
    
    if window_dim < 2
        window_dim = randint(1, 1, [2 inst_dim*0.5])
    end
    
    window_length = inst_dim*window_dim - (inst_dim-window_dim);
    window_row = [ones(1, window_dim) zeros(1, inst_dim-window_dim)];
    
    window_full = repmat(window_row, window_dim, 1); 
    window_full = window_full'(:)(1:window_length);
    
    focus_idx = randint(1, 1, [1 feature_n-window_length+1]);

    window_mask = zeros(1, feature_n);
    window_mask(focus_idx:focus_idx+window_length-1) = window_full;
    
endfunction
