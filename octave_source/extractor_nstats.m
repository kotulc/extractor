## Copyright (C) 2016 kotulc
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} extractor_nstats(@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: kotulc <kotulc@KOTULC-PC>
## Created: 2016-08-08

function [excite_actvs, inhibit_actvs] = extractor_nstats(train_data, node_weights, excite_idx=[], inhibit_idx=[])

    if numel(excite_idx) != 0
        excite_actvs = extractor_fprop(...
                {node_weights}, train_data.excite_actvs(excite_idx, :)).^2;
    else
        excite_actvs = extractor_fprop(...
                {node_weights}, train_data.excite_actvs).^2;
    end
    
    if numel(inhibit_idx) != 0
        inhibit_actvs = extractor_fprop(...
                {node_weights}, train_data.inhibit_actvs(inhibit_idx, :)).^2;
    else
        inhibit_actvs = extractor_fprop(...
                {node_weights}, train_data.inhibit_actvs).^2;
    end
            
    excite_sum = sum(excite_actvs);
    excite_n = size(excite_actvs, 1);

    inhibit_sum = sum(inhibit_actvs);
    inhibit_n = size(inhibit_actvs, 1);  
    
    ratio = excite_sum / inhibit_sum;
    s_ratio = excite_sum / (inhibit_sum * (excite_n/inhibit_n));

    print_str = "n_stats (%d/%d instances) e/i - sum: %d/%d ";
    print_str = [print_str, "gtr_0.5: %d/%d, ratio/scaled: %d/%d\n"];
    fprintf(print_str, excite_n, inhibit_n, excite_sum, inhibit_sum,... 
            sum(excite_actvs > 0.5), sum(inhibit_actvs > 0.5), ratio, s_ratio);  
    
endfunction
